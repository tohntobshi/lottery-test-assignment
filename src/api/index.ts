import { delay } from 'redux-saga/effects'
import { BACKEND_ADDRESS } from '../config'

export async function retry<Fn extends (...args: any[]) => Promise<any>>(functionToCall: Fn, attempts: number, ...args: Parameters<Fn>): Promise<UnpackedReturnedPromise<Fn>> {
  let currentAttempts = 0
  const makeRequest = async (): Promise<UnpackedReturnedPromise<Fn>> => {
    try {
      const result = await functionToCall(...args)
      return result
    } catch (e) {
      if (currentAttempts === attempts - 1) {
        throw (e)
      }
      currentAttempts++
      await delay(200)
      return makeRequest()
    }
  }
  return makeRequest()
}

export async function sendResult(upperNumbers: number[], lowerNumbers: number[], isSuccess: boolean): Promise<void> {
  const willFail = Math.random() > 0.8
  const dataToSend = {
    selectedNumber: {
      firstField: upperNumbers,
      secondField: lowerNumbers
    },
    isTicketWon: isSuccess
  }
  return fetch(`${BACKEND_ADDRESS}${willFail ? '/500' : '/200'}`, {
    method: 'POST',
    body: JSON.stringify(dataToSend)
  }).then((res) => {
    if (!res.ok) {
      throw new Error('Something went wrong')
    }
  })
}

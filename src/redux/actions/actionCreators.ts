import * as Actions from './actionTypes'

export const setNumbers = (numbers: number[] | number, which: 'upper' | 'lower') => ({ type: Actions.SET_NUMBERS, numbers, which })
export const setPending = (state: boolean) => ({ type: Actions.SET_PENDING, state })
export const setResult = (text: string) => ({ type: Actions.SET_RESULT, text })
export const reset = () => ({ type: Actions.RESET })
export const requestResult = () => ({ type: Actions.REQUEST_RESULT })

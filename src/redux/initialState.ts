import { RootState } from ':types'

export const INITIAL_STATE: RootState = {
  upperNumbers: [],
  lowerNumbers: [],
  pending: false,
  result: ''
}

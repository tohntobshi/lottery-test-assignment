import { createReducer } from 'reduxsauce'
import { AnyAction } from 'redux'
import * as Actions from ':actions'
import { RootState } from ':types'
import { INITIAL_STATE } from '../initialState'

function setNumbers(state: RootState, action: ReturnType<typeof Actions.setNumbers>): RootState {
  const which = action.which === 'upper' ? 'upperNumbers' : 'lowerNumbers'
  const maxLength = action.which === 'upper' ? 8 : 1
  if (Array.isArray(action.numbers)) {
    return {
      ...state,
      [which]: action.numbers.slice(0, maxLength)
    }
  } else {
    const newNumbers = state[which].includes(action.numbers)
      ? state[which].filter(el => el !== action.numbers)
      : [...state[which], action.numbers].slice(0, maxLength)
    return {
      ...state,
      [which]: newNumbers
    }
  }
}

function setPending(state: RootState, action: ReturnType<typeof Actions.setPending>): RootState {
  return {
    ...state,
    pending: action.state
  }
}

function setResult(state: RootState, action: ReturnType<typeof Actions.setResult>): RootState {
  return {
    ...state,
    result: action.text
  }
}

function reset(state: RootState, action: ReturnType<typeof Actions.reset>): RootState {
  return INITIAL_STATE
}

const HANDLERS = {
  [Actions.SET_NUMBERS]: setNumbers,
  [Actions.SET_PENDING]: setPending,
  [Actions.SET_RESULT]: setResult,
  [Actions.RESET]: reset
}

export default createReducer<RootState, AnyAction>(INITIAL_STATE, HANDLERS)

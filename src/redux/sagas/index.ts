import { takeLatest, select, put, call } from 'redux-saga/effects'
import * as Actions from ':actions'
import { RootState } from ':types'
import { generateAndCompare } from ':utils'
import { sendResult, retry } from ':api'

export function * requestResult(action: ReturnType<typeof Actions.requestResult>) {
  yield put(Actions.setPending(true))
  const { upperNumbers, lowerNumbers }: RootState = yield select()
  const isSuccess = generateAndCompare(upperNumbers, lowerNumbers)
  try {
    const whateverResult: UnpackedReturnedPromise<typeof sendResult> = yield call(retry, sendResult, 3, upperNumbers, lowerNumbers, isSuccess)
    if (isSuccess) {
      yield put(Actions.setResult('Ого, вы виграли! Поздравляем!'))
    } else {
      yield put(Actions.setResult('Вы проиграли. Попробуйте снова.'))
    }
  } catch (e) {
    yield put(Actions.setResult(e.message))
  }
  yield put(Actions.setPending(false))
}

export default function * rootSaga() {
  yield takeLatest(Actions.REQUEST_RESULT, requestResult)
}

import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension'
import { INITIAL_STATE } from './initialState'
import reducers from './reducers'
import sagas from './sagas'

const sagaMiddleware = createSagaMiddleware()

const composeEnhancers = composeWithDevTools({
  features: {
    persist: false
  }
})

export const store = createStore(
  reducers,
  INITIAL_STATE,
  composeEnhancers(applyMiddleware(sagaMiddleware))
)

sagaMiddleware.run(sagas)

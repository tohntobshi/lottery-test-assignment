import React from 'react'
import { connect } from 'react-redux'
import * as Actions from ':actions'
import { RootState } from ':types'
import classnames from 'classnames'
import { NUMBERS_TO_CHOOSE, NUMBERS_TO_CHOOSE_2 } from ':constants'
import { generateUniqRandomNumberArray } from ':utils'
const styles = require('./styles.scss')

const mapStateToProps = ({
  upperNumbers,
  lowerNumbers,
  pending,
  result
}: RootState) => ({
  upperNumbers,
  lowerNumbers,
  pending,
  result
})

const mapDispatchToProps = {
  setNumbers: Actions.setNumbers,
  requestResult: Actions.requestResult,
  reset: Actions.reset
}

type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps

const Sample = ({
  upperNumbers,
  lowerNumbers,
  pending,
  reset,
  result,
  setNumbers,
  requestResult
}: Props) => {
  const randomize = React.useCallback(() => {
    setNumbers(generateUniqRandomNumberArray(1, 18, 8), 'upper')
    setNumbers(generateUniqRandomNumberArray(1, 2, 1), 'lower')
  }, [])
  return (
    <div className={classnames('transitional', styles.background)}>
      <div className={styles.container}>
        {
          pending &&
          <div className='spinnerBackground'>
            <div className='spinner'/>
          </div>
        }
        <div>

          <div className={styles.head}>
            <h1 className='text16'>Билет 1</h1>
            {
              !result && <button className='btn1' onClick={randomize}></button>
            }
          </div>
          {
            result
              ? <p className='text14Light'>{result}</p>
              : <>
                <p className='text14'>Поле 1 <span className='text14Light'>Отметьте 8 чисел</span></p>
                <div className={styles.numbersContainer}>
                  {
                    NUMBERS_TO_CHOOSE.map((el) =>
                      <button
                        className={classnames('btn2', upperNumbers.includes(el) && 'active')}
                        key={el}
                        onClick={() => setNumbers(el, 'upper')}
                      >{el}</button>)
                  }
                </div>
                <p className='text14'>Поле 2 <span className='text14Light'>Отметьте 1 число</span></p>
                <div className={styles.numbersContainer}>
                  {
                    NUMBERS_TO_CHOOSE_2.map((el) =>
                      <button
                        className={classnames('btn2', lowerNumbers.includes(el) && 'active')}
                        key={el}
                        onClick={() => setNumbers(el, 'lower')}
                      >{el}</button>)
                  }
                </div>
              </>
          }
        </div>
        <div className={styles.btnContainer}>
          {
            result
              ? <button className={classnames('text14Light', 'btn3')} onClick={reset}>Попробовать снова</button>
              : <button className={classnames('text14Light', 'btn3')} onClick={requestResult}>Показать результат</button>
          }
        </div>
      </div>
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Sample)

export interface RootState {
  upperNumbers: number[];
  lowerNumbers: number[];
  pending: boolean;
  result: string;
}

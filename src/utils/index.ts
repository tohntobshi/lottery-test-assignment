export function generateRandomNumber(minVal: number, maxVal: number): number {
  const diff = maxVal - minVal
  return minVal + Math.round(diff * Math.random())
}

export function generateUniqRandomNumberArray(minVal: number, maxVal: number, amount: number): number[] {
  const numbers: number[] = []
  const addNumber = (): void => {
    const newNumber = generateRandomNumber(minVal, maxVal)
    if (numbers.includes(newNumber)) {
      return addNumber()
    } else {
      numbers.push(newNumber)
    }
  }
  for (let i = 0; i < amount; i++) {
    addNumber()
  }
  return numbers
}

export function generateAndCompare(upperNumbers: number[], lowerNumbers: number[]): boolean {
  const upperNumbersToCompare = generateUniqRandomNumberArray(1, 19, 8)
  const lowerNumbersToCompare = generateUniqRandomNumberArray(1, 2, 1)
  let matches = 0
  for (const number of upperNumbers) {
    if (upperNumbersToCompare.includes(number)) {
      matches++
    }
  }
  for (const number of lowerNumbers) {
    if (lowerNumbersToCompare.includes(number)) {
      matches++
    }
  }
  return matches >= 4
}
